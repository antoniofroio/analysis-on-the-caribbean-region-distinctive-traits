# Analysis on the Caribbean region distinctive traits

This analysis will:

1. Compare the region's most distinctive traits in comparison to the rest of the world.
2. Select a country which best reflects the region's features on average.